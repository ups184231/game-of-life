# Stage One
FROM maven:3.8.6-openjdk-8 as build
ARG BRANCH=master
RUN git clone https://github.com/wakaleo/game-of-life.git && cd game-of-life && \
	git checkout ${BRANCH} && mvn package

# Stage Two
FROM tomcat:jre8-alpine
LABEL project="game_of_life"
LABEL author="wakaleo"
EXPOSE 8080
COPY --from=build /game-of-life/gameoflife-web/target/gameoflife.war /usr/local/tomcat/webapps/gameoflife.war
CMD ["catalina.sh", "run"]